<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include 'db_connection.php';
include 'queries.php';
$pass = $_REQUEST["dbPass"];//Login Password for database. Always needed. Will later ensure encryption
$conn = OpenCon($pass);
$type=$_REQUEST["type"];//Code for which function is called. Always needed. Will later try to extend into an option to leave
switch($type) {
    case 'SelectAllEvent'://Selects names of events for given member.
        $output=array();
        $MemberID=$_REQUEST["MemberID"]; //PK for members
        $sql = "SELECT * FROM EVENTS ev INNER JOIN MEMBER_EVENT me on me.EVENT_ID = ev.EVENT_ID where me.MEMBER_ID = '$MemberID'";
        if ($result = mysqli_query($conn, $sql)) {
            while ($row = mysqli_fetch_array($result)) {
                $output[] = $row;
            }
            echo json_encode($output);
            mysqli_free_result($result);
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        break;
    case 'Register'://Insert new member
        $name = $_REQUEST["name"];//Login name for member
        $Password=$_REQUEST["Password"];//Password for memeber. Needs to be stored as encyted (Later)
        $email = $_REQUEST["email"];//Login email for member. Login using email because of uniqueness.
        $sql = "INSERT INTO MEMBER(NAME, PASSWORD, EMAIL) VALUES ('$name', '$Password', '$email')";
        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        break;
    case 'login'://Login Member
        $output=array();
        $email = $_REQUEST["email"];//Login email for member. Login using email because of uniqueness.
        $Password=$_GET["Password"];//Password for memeber. Needs to be stored as encyted (Later)
        $sql = "Select * from MEMBER where PASSWORD = '$Password' and EMAIL = '$email'";
        if ($result = mysqli_query($conn, $sql)) {
            if(mysqli_num_rows($result) > 0 ){
                while ($row = mysqli_fetch_array($result)) {
                    $output[] = $row;
                }
                echo json_encode($output);//Used to get MEMBER_ID for future work
            }
            else{
                echo json_encode("Incorrect Login");
            }
            mysqli_free_result($result);
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        break;
    case 'SelectIndEvent'://Select all details of a specific event
        $EID=$_GET["E_ID"];//EventID
        $sql = "SELECT * FROM EVENTS  where EVENT_ID='$EID'";
        $output=array();
        if ($result = mysqli_query($conn, $sql)) {
            while ($row = mysqli_fetch_array($result)) {
                $output[] = $row;
            }
            echo json_encode($output);
            mysqli_free_result($result);
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        break;
    case 'CreateEvent'://Create a new event for a member
        $MemberID=$_REQUEST["MemberID"];//Identify memeber Making the event
        $name=$_REQUEST["EventName"];//Name of the event. Cannot be null
        $sql = "INSERT INTO EVENTS(EVENT_NAME) VALUES('$name')";//Insert into table EVENTS
        if ($conn->query($sql) === TRUE) {
            $sql = "SELECT EVENT_ID FROM EVENTS where EVENT_NAME = '$name' ORDER BY EVENT_ID DESC LIMIT 1";//GET EVENT_ID of new event
            if ($result=mysqli_query($conn, $sql)) {
                if(mysqli_num_rows($result) > 0 ) {//Checking if their is a result
                    while ($row = mysqli_fetch_array($result)) {//Can only be one due to TOP 1
                        $EventID = $row['EVENT_ID'];
                    }
                    mysqli_free_result($result);
                    $sql = "INSERT INTO MEMBER_EVENT(MEMBER_ID, EVENT_ID,CODE,VERIFIED) VALUES('$MemberID','$EventID','1452','1')";//Insert into joining table
                    if ($conn->query($sql) === TRUE) {
                        echo "New record created successfully";
                    } else {
                        echo "Error: " . $sql . "<br>" . $conn->error;
                    }
                }else{
                    echo "Incorrect Parameters";
                }
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        break;
    case 'SetDetail'://Sets a value to an individual detail in the event
        $Name=$_REQUEST["ChangeEvent"];//Detail to be changed
        $Value=$_REQUEST["Value"];//Value of Detail
        $EID=$_REQUEST["E_ID"];//EventID
        $sql = "UPDATE EVENTS SET $Name='$Value' WHERE EVENT_ID='$EID'";
        if ($conn->query($sql) === TRUE) {
            echo "Succesful Update";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        break;
    case 'SelectAllContacts'://Request for all contacts associated with a member
        $MemberID=$_REQUEST["MemberID"];
        $sql = "SELECT * FROM CONTACTS  where MEMBER_ID='$MemberID'";
        $output=array();
        if ($result = mysqli_query($conn, $sql)) {
            while ($row = mysqli_fetch_array($result)) {
                $output[] = $row;
            }
            echo json_encode($output);
            mysqli_free_result($result);
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        break;
    case 'AddContactToEvent'://Adds a contact to a specific event
        $GuestID=$_REQUEST["GUEST_ID"];//Guest to be added
        $EventID=$_REQUEST["Event_ID"];//Event invited to.
        // $BRING=$_REQUEST["BringItem"];//Stuff to bring. Please add a pre-defined list in java and note ypu may need to pass in null
        //$Status=$_REQUEST["GuestStatus"]; Reply status of guest default is "no reply" so no need to send details
        $sql = "INSERT INTO GUEST_EVENT(GUEST_ID, EVENT_ID) VALUES('$GuestID','$EventID')";//Insert into joining table
        if ($conn->query($sql) === TRUE) {
            echo "New record created succesfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        break;
    case 'SelectEventDetails'://Selects all the details of a specific event including guests for one guest to be sent to the API
        $EventID=$_REQUEST["EventID"];
        $sql = "SELECT  * from EVENTS e INNER JOIN GUEST_EVENT ge on ge.EVENT_ID = e.EVENT_ID INNER JOIN CONTACTS c where c.CONTACT_ID=ge.GUEST_ID AND e.EVENT_ID = '$EventID'";
        $output=array();
        if ($result = mysqli_query($conn, $sql)) {
            while ($row = mysqli_fetch_array($result)) {
                $output[] = $row;
            }
            echo json_encode($output);
            mysqli_free_result($result);
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        break;
      case 'AddBring'://Details what the guest must bring
          $GuestID=$_REQUEST["GUEST_ID"];//Guest to be bring
          $EventID=$_REQUEST["Event_ID"];//Event to bring to
          $BRING=$_REQUEST["BringItem"];//Item to bring
          $sql = "UPDATE GUEST_EVENT SET BRING = '$BRING' where GUEST_ID='$GuestID' AND EVENT_ID='$EventID'";
          if ($conn->query($sql) === TRUE) {
              echo "Record updated succesfully";
          } else {
              echo "Error: " . $sql . "<br>" . $conn->error;
          }
          break;
    case 'AddContact'://Adds a contact to a specific event
        $name=$_REQUEST["Name"];
        $email=$_REQUEST["Email"];
        $MemberID=$_REQUEST["MemberID"];

        $sql = "INSERT INTO contacts(CONTACT_NAME, EMAIL,MEMBER_ID) VALUES('$name','$email','$MemberID')";//Insert into joining table
        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        break;
        case 'SendVerification'://Sends an email to the user with the verification code
        require 'PHPMailer/src/Exception.php';
        require 'PHPMailer/src/PHPMailer.php';
        require 'PHPMailer/src/SMTP.php';
        $name = $_GET["name"]; //name of new memeber
        $email = $_GET["email"];//email used to send
        $code = $_GET["code"];//Verification code generated on android side
        $mail = new PHPMailer();
        try {
            $mail->SMTPDebug = 1;                                       // Enable verbose debug output
            $mail-> isSMTP();
            $mail-> SMTPAuth =true;
            $mail-> SMTPSecure='ssl';
            $mail-> Host='smtp.gmail.com';
            $mail-> Port='465';
            $mail-> isHTML();
            $mail-> Username='gatsby.event.manager@gmail.com';
            $mail-> Password='GEadjjs2019';
    
            $mail-> SetFrom('no-reply@gatsby.com');
            $mail->AddAddress($email,$name);
        
            $mail-> Subject='Verify Account';
            $mail-> Body="Hello and welcome to Gatsby. \n To verify your account, please enter the verification code into the Gatsby App.\n Your verification code is: $code";
        
            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
        break;
    case 'DeleteEvent'://Delets an event and all associated with it.
        $EventID=$_REQUEST["EventID"];
        $sql="DELETE from MEMBER_EVENT WHERE EVENT_ID='$EventID'";
        if ($conn->query($sql) === TRUE) {
            echo "Member_Event Record delelted succesfully";
            $sql="DELETE from GUEST_EVENT WHERE EVENT_ID='$EventID'";
            if ($conn->query($sql) === TRUE) {
                echo "Guest_Event Record delelted succesfully";
                $sql="DELETE from EVENTS WHERE EVENT_ID='$EventID'";
                if ($conn->query($sql) === TRUE) {
                    echo "Event Record delelted succesfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    break;
    default:
        echo json_encode("Incorrect Parameter for type");

}
CloseCon($conn);
?>
