-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2019 at 05:16 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gatsby`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `CONTACT_ID` int(11) NOT NULL,
  `CONTACT_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MEMBER_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `EVENT_ID` int(11) NOT NULL,
  `EVENT_NAME` varchar(100) NOT NULL,
  `DATE` date DEFAULT NULL,
  `START_TIME` time DEFAULT NULL,
  `END_TIME` time DEFAULT NULL,
  `VENUE` varchar(100) DEFAULT NULL,
  `PARKING` varchar(500) DEFAULT NULL,
  `DRESS_CODE` varchar(100) DEFAULT NULL,
  `DRINKS` tinyint(1) DEFAULT NULL,
  `RSVP` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`EVENT_ID`, `EVENT_NAME`, `DATE`, `START_TIME`, `END_TIME`, `VENUE`, `PARKING`, `DRESS_CODE`, `DRINKS`, `RSVP`) VALUES
(1, 'Birthday1', '2019-08-31', '05:03:00', '12:00:00', 'My House', 'Please park in the street', 'Formal', 0, '2019-08-23'),
(2, 'Birthday2', '2019-08-22', '09:02:02', '19:00:00', 'The local park', 'Park in the allocated space', NULL, 1, '2019-08-12'),
(3, 'Family Get together', '2019-08-30', NULL, NULL, 'Gran\'s House', NULL, NULL, NULL, NULL),
(4, 'Planned Birthday', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'School Renion', '2019-09-20', '12:00:00', NULL, 'Our old school', 'Old Matric Parking', 'Business formal', 0, '2019-08-22'),
(6, 'PartyTime', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'PartyTime', NULL, NULL, NULL, 'My Houses', NULL, NULL, NULL, NULL),
(8, 'PartyTime', NULL, NULL, NULL, 'My House', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `guest_event`
--

CREATE TABLE `guest_event` (
  `GUEST_ID` int(11) NOT NULL,
  `EVENT_ID` int(11) NOT NULL,
  `BRING` varchar(100) DEFAULT NULL,
  `STATUS` varchar(100) NOT NULL DEFAULT 'no reply'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `MEMBER_ID` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`MEMBER_ID`, `NAME`, `PASSWORD`, `EMAIL`) VALUES
(0, 'David Wright', 'CricketSA', 'davidwright@gmail.com'),
(1, 'John Smith', 'ThisIsAPassword', 'genericname@gmail.com'),
(2, 'Chris Robinson', 'AnotherPassword', 'anotheremail@gmail.com'),
(342, 'John Stewart', 'Passthison', 'randomnes@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `member_event`
--

CREATE TABLE `member_event` (
  `ME_ID` int(11) NOT NULL,
  `MEMBER_ID` int(11) NOT NULL,
  `EVENT_ID` int(11) NOT NULL,
  `CODE` int(11) NOT NULL,
  `VERIFIED` tinyint(1) NOT NULL DEFAULT 0,
  `BRING` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_event`
--

INSERT INTO `member_event` (`ME_ID`, `MEMBER_ID`, `EVENT_ID`, `CODE`, `VERIFIED`, `BRING`) VALUES
(1, 0, 5, 1234, 1, NULL),
(2, 1, 5, 0, 1, 'Champagne'),
(3, 0, 1, 2356, 1, NULL),
(4, 2, 2, 23678, 0, NULL),
(5, 1, 3, 3562, 1, NULL),
(6, 1, 4, 1452, 1, NULL),
(7, 2, 6, 1452, 1, NULL),
(8, 2, 8, 1452, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`CONTACT_ID`),
  ADD KEY `MEMBER_ID` (`MEMBER_ID`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`EVENT_ID`);

--
-- Indexes for table `guest_event`
--
ALTER TABLE `guest_event`
  ADD KEY `EVENT_ID` (`EVENT_ID`),
  ADD KEY `GUEST_ID` (`GUEST_ID`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`MEMBER_ID`);

--
-- Indexes for table `member_event`
--
ALTER TABLE `member_event`
  ADD PRIMARY KEY (`ME_ID`),
  ADD KEY `MEMBER_ID` (`MEMBER_ID`),
  ADD KEY `Event_ID` (`EVENT_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `CONTACT_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `EVENT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `MEMBER_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343;

--
-- AUTO_INCREMENT for table `member_event`
--
ALTER TABLE `member_event`
  MODIFY `ME_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`MEMBER_ID`) REFERENCES `member` (`MEMBER_ID`);

--
-- Constraints for table `guest_event`
--
ALTER TABLE `guest_event`
  ADD CONSTRAINT `guest_event_ibfk_1` FOREIGN KEY (`EVENT_ID`) REFERENCES `events` (`EVENT_ID`),
  ADD CONSTRAINT `guest_event_ibfk_2` FOREIGN KEY (`GUEST_ID`) REFERENCES `contacts` (`CONTACT_ID`);

--
-- Constraints for table `member_event`
--
ALTER TABLE `member_event`
  ADD CONSTRAINT `member_event_ibfk_1` FOREIGN KEY (`MEMBER_ID`) REFERENCES `member` (`MEMBER_ID`),
  ADD CONSTRAINT `member_event_ibfk_2` FOREIGN KEY (`Event_ID`) REFERENCES `events` (`EVENT_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
