package com.example.gatsby;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragmentAdditionalDetails extends Fragment {
    private static final String TAG = "Additional Details tab";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_more_details,container,false);
        Events currentEvent;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson;

        gsonBuilder.setDateFormat("dd-mm-yyyy");
        gson = gsonBuilder.create();
        try{
            String Event = getArguments().getString("event");
            currentEvent = gson.fromJson(Event.toString(), Events.class);
            currentEvent.setJsonDetails(Event);
        }catch (Exception e){
            return view;
        }
        ArrayList<String> EventDetails = new ArrayList<>();
        EventDetails.add(currentEvent.getParkingDetails());
        EventDetails.add(currentEvent.getDressCode());
        EventDetails.add(currentEvent.getRSVP());
        EventDetails.add(Character.toString(currentEvent.getDrinks()));

        final RecyclerView GenDetailsRV;
        GenDetailsRV = view.findViewById(R.id.gen_details_rv);
        GenDetailsRV.setLayoutManager(new LinearLayoutManager(getActivity()));

        GenDetailsRV.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
        GenDetailsRV.setVisibility(View.VISIBLE);
        GenDetailsRV.setAdapter(new AdditionalDetailsAdapter(getActivity(),EventDetails,Integer.toString(currentEvent.getId()),currentEvent));
        return view;
    }
}

