package com.example.gatsby;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import static android.content.Context.MODE_PRIVATE;

public class FragmentGuestList extends Fragment {
    private static final String TAG = "Additional Details tab";
    private ArrayList<Guest> guestList = new ArrayList<>();
    Events currentEvent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final  View view2 = inflater.inflate(R.layout.fragment_guestlist,container,false);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson;

        gsonBuilder.setDateFormat("dd-mm-yyyy");
        gson = gsonBuilder.create();

        try{
            if (getArguments() != null) {//this needs to be here otherwise a null pointer exception will always be triggered
                String Event = getArguments().getString("event");
                currentEvent = gson.fromJson(Event.toString(), Events.class);
                currentEvent.setJsonDetails(Event);
              //  Log.d("niks2",currentEvent.getDate());
            }else{
             //   Log.d("iets","its null");
            }
          //
           //
        }catch (Exception e){
            Log.d("something",e.getMessage());

//            Toast.makeText(getActivity(),"guest page problem",Toast.LENGTH_LONG).show();
            return view2;
        }
        FloatingActionButton createEvent = view2.findViewById(R.id.floatingActionButton);
        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddContact(view2);
//                Snackbar.make(view, "select the contacts that you wish to add", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

            }
        });

        updateGuestList(view2);
        //next we get a list of guests

        return view2;
    }
    void updateGuestList(final View view){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        SharedPreferences WebAccess = getContext().getSharedPreferences("Web_Access",MODE_PRIVATE);
        String url =WebAccess.getString("Root_URL",null)+"?type=SelectEventDetails&dbPass=&EventID="+currentEvent.getId();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //


                        Gson gson;
                        Log.d("result",response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("dd-mm-yyyy");
                        gson = gsonBuilder.create();


                        try{
                            JSONArray jsonGuestArray = new JSONArray(response);/*"[{\"CONTACT_NAME\":\"Agang\",\n" +
                                                                    "\"SURNAME\":\"Lebethe\"\n" +
                                                                    "},\n" +
                                                                    "{\"NAME\":\"David\",\n" +
                                                                    "\"SURNAME\":\"Wright\"\n" +
                                                                    "}\n" +
                                                                    "]");//response);*/
                            guestList.clear();
                            for (int item = 0; item<jsonGuestArray.length(); item++){
                                JSONObject Event = (JSONObject)jsonGuestArray.get(item);
                                Guest newGuest = gson.fromJson(Event.toString(),Guest.class);
                                guestList.add(newGuest);

                            }
                            final RecyclerView guestRV;
                            guestRV = view.findViewById(R.id.gen_details_rv);
                            guestRV.setLayoutManager(new LinearLayoutManager(getActivity()));
                            guestRV.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
                            guestRV.setVisibility(View.VISIBLE);
                            guestRV.setAdapter(new GuestlistAdapter(getActivity(),guestList,currentEvent));//,Integer.toString(currentEvent.getId()),currentEvent));
//                            Toast.makeText(getContext(),guestList.get(0).getF_name()+"|",Toast.LENGTH_LONG).show();

                        }catch(Exception e){
                           // Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();
                            Log.d("problem13",e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO Replace this with snackbar
//                Toast.makeText(getContext(),"An error occurred",Toast.LENGTH_LONG).show();
                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);
    }
    void AddContact(final View view){
//        updateGuestList(view);
        //first we get a list of all a person's contacts
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        SharedPreferences WebAccess = getContext().getSharedPreferences("Web_Access",MODE_PRIVATE);
        String url =WebAccess.getString("Root_URL",null)+"?type=SelectAllContacts&dbPass="+"&MemberID="+currentEvent.getMemberid();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //


                        Gson gson;
                        Log.d("result",response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("dd-mm-yyyy");
                        gson = gsonBuilder.create();

                        final ArrayList<String> contactNames = new ArrayList<>();
                        final ArrayList<Boolean> checkedNames = new ArrayList<>();

                        try{

                            final ArrayList<Guest> Contacts = new ArrayList<>();
                            JSONArray jsonContactArray = new JSONArray(response);/*"[{\"NAME\":\"James\",\n" +
                                    "\"SURNAME\":\"Lebreezy\"\n" +
                                    "},\n" +
                                    "{\"NAME\":\"Steve\",\n" +
                                    "\"SURNAME\":\"Jobs\"\n" +
                                    "}\n" +
                                    "]");//response);*/
                            Contacts.clear();
                            final boolean [] checkedList2 = new boolean[jsonContactArray.length()] ;
                            Arrays.fill(checkedList2,false);
                            for (int item = 0; item<jsonContactArray.length(); item++){
                                JSONObject Contact = (JSONObject)jsonContactArray.get(item);
                                Guest newGuest = gson.fromJson(Contact.toString(),Guest.class);
                                if (guestList.contains(newGuest)){
                                    checkedList2[item] = true;
                                }
                                Contacts.add(newGuest);
                                contactNames.add(Contacts.get(item).getName());
                                checkedNames.add(false);

                            }

                            //once we have all their contacts allow them to change ,
                            final AlertDialog.Builder ContactDialogue = new AlertDialog.Builder(getActivity(),R.style.AppTheme);
                            ContactDialogue.setTitle("Add contact to Guest list");


                            String[] nameList = contactNames.toArray(new String[contactNames.size()]);
                            final boolean [] checkedList = new boolean[nameList.length] ;
                            Arrays.fill(checkedList,false);


                            ContactDialogue.setMultiChoiceItems(nameList,checkedList2, new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                    checkedList[which] = isChecked;

                                }
                            });


                            ContactDialogue.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                   final RequestQueue queue = Volley.newRequestQueue(getActivity());




                                    for (int j = 0; j<checkedList.length; j++){
                                        if (checkedList[j] ){//if the contact was checked, then add them to the guest list
                                            //TODO FIX THE EVENT ID
                                            SharedPreferences WebAccess = getContext().getSharedPreferences("Web_Access",MODE_PRIVATE);
                                            String url =WebAccess.getString("Root_URL",null)+"?type=AddContactToEvent&dbPass="+"&Event_ID="+currentEvent.getId()+"&GUEST_ID="+Contacts.get(j).getcID();
                                            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            //TODO ADD A SNACKBAR
                                                            if (response.contains("New record created succesfully")){
                                                                updateGuestList(view);
                                                            }
//                                                            Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();
                                                           // Log.d("responseg", response);
                                                        }
                                                    }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    //TODO Replace this with snackbar
                                                    Toast.makeText(getContext(),"An error occurred",Toast.LENGTH_LONG).show();
                                                    try {
                                                        Log.d("failed", error.getMessage());
                                                    }catch(Exception e){
                                                        Log.d("last", e.getMessage());
                                                    }
                                                }
                                            });
                                            queue.add(stringRequest);
                                        }
//                                        Log.d("checked",contactNames.get(j));

                                    }
                                    updateGuestList(view);
                                }
                            });
                            ContactDialogue.setNegativeButton("Cancel", null);

                            ContactDialogue.show();

                        }catch(Exception e){
                            Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();
                            Log.d("problem4",e.getMessage()+response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO Replace this with snackbar
                Toast.makeText(getContext(),"An error occurred",Toast.LENGTH_LONG).show();
                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);
    }
    //TODO save person's contacts on the app (get them  when they log in and whenever they update their contacts, the list
    // should also be updated)
    //TODO get a list of contacts that aren't invited yet
}

