package com.example.gatsby;

import com.google.gson.annotations.SerializedName;

public class Events {




    @SerializedName("EVENT_ID")
        private int id;
        @SerializedName("EVENT_NAME")
        private String name;
        @SerializedName("MEMBER_ID")
        private String memberid;
        @SerializedName("START_TIME")
        private String startTime;
        @SerializedName("END_TIME")
        private String endTime;
        @SerializedName("VENUE")
        private String venue;
        @SerializedName("DRESS_CODE")
        private String dressCode;
        @SerializedName("PARKING")
        private String parkingDetails;
        @SerializedName("RSVP")
        private String RSVP;
        @SerializedName("DATE")
        private String date;
        @SerializedName("DRINKS")
        private char Drinks;


        private String jsonDetails; //The details in json format
         Events(String name){
            this.name = name;
        }

     String getJsonDetails() {
        return jsonDetails;
    }

     void setJsonDetails(String jsonDetails) {
        this.jsonDetails = jsonDetails;
    }

    public int getId() {
        return id;
    }



    public String getName() {
        return name;
    }



     String getStartTime() {
        return startTime;
    }



    String getEndTime() {
        return endTime;
    }



    String getVenue() {
        return venue;
    }



     String getDressCode() {
        return dressCode;
    }



     String getParkingDetails() {
        return parkingDetails;
    }


     String getRSVP() {
        return RSVP;
    }



     String getDate() {
        return date;
    }



     char getDrinks() {
        return Drinks;
    }

    public void setRSVP(String RSVP) {
        this.RSVP = RSVP;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMemberid() {
        return memberid;
    }

}
