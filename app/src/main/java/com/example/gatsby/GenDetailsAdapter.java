package com.example.gatsby;

import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textview.MaterialTextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class GenDetailsAdapter extends RecyclerView.Adapter<GenDetailsAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<String> GenDetails;
    private String EventID;
    private Events currentEvent;

     GenDetailsAdapter(Context context,ArrayList<String> GenDetails,String EventID,Events currentEvent) {

        this.context = context;
        this.GenDetails = GenDetails;
        this.EventID = EventID;
        this.currentEvent = currentEvent;
    }

    @Override
    public void onBindViewHolder(final @NonNull GenDetailsAdapter.MyViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch(position){
                    case 0:
                    case 4:
                        editDialog(GenDetails.get(position),position,holder);
                        break;
                    case 1:
                        DatePicker(holder,GenDetails.get(position));
                        break;
                    case 2:
                    case 3:
                        TimePicker(holder,position,GenDetails.get(position));
                        break;
                    default:
                        break;


                }

            }
        });
        switch(position){
            case 0:
               holder.Description.setText(context.getString(R.string.event_name));
               break;
            case 1:
                holder.Description.setText(context.getString(R.string.date));
                break;
            case 2:
                holder.Description.setText(context.getString(R.string.start_time));
                break;
            case 3:
                holder.Description.setText(context.getString(R.string.end_time));
                break;
            case 4:
                holder.Description.setText(context.getString(R.string.venue));
                break;

            default:break;
        }

        holder.genDetail.setText(GenDetails.get(position));
    }

    @Override
    public  @NonNull
    GenDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.rv_details,parent,false);


        return new MyViewHolder(v);
    }


     class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView Description,genDetail ;

            MyViewHolder(View itemView) {
            super(itemView);
            Description = itemView.findViewById(R.id.Description);
            genDetail = itemView.findViewById(R.id.genDetail);

        }
    }

    @Override
    public int getItemCount() {
        return GenDetails.size();
    }


    private void editDialog(String CurrString,final int Code,final MyViewHolder holder){
        /*
        This dialog will pop up giving the user the option to change
        the information
         */
        String title="",message="",Detail="";
        switch (Code){
            case 0:
                title = "Event Name";
                message = "Change event Name";
                Detail = "EVENT_NAME";
                break;
            case 4:
                title = "Venue";
                message = "Change Venue";
                Detail = "VENUE";
                break;
            default:
                break;
        }
        final String fDetail = Detail;
        AlertDialog.Builder  dlgUpdate= new  AlertDialog.Builder(context, R.style.AppTheme);
        final EditText edtUpdate= new EditText(context);
        edtUpdate.setText(CurrString);
        edtUpdate.setSelection(edtUpdate.getText().length());

        dlgUpdate.setMessage(message);
        dlgUpdate.setTitle(title);

        dlgUpdate.setView(edtUpdate);

        dlgUpdate.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*
                Logic to update the information
                 */
                //EventName.text

                ChangeDetail(edtUpdate.getText().toString(),fDetail,holder);
                //Toast.makeText(context,"pressed",Toast.LENGTH_LONG).show();


            }
        });

        dlgUpdate.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dlgUpdate.show();
    }

    private void DatePicker(final MyViewHolder holder, final String current){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final DatePicker picker = new DatePicker(context);
        picker.setCalendarViewShown(false);
        builder.setTitle("Change Date");
        builder.setView(picker);

        if (currentEvent.getRSVP()!=null) {

            String someDate = currentEvent.getRSVP();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            try{
                Date date = sdf.parse(someDate);
                picker.setMinDate(date.getTime());//-10*((long)Math.pow(10,10)));
                Log.d("date", "fine date");
            }catch(Exception e){
                Log.d("wrong date", e.getMessage());
            }


        }

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*
                Logic to update the information
                 */
                int day = picker.getDayOfMonth();
                int month = (picker.getMonth());
                int year =  picker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String dateString = sdf.format(calendar.getTime());
                if (!dateString.equals(current)) {

                    ChangeDetail(dateString, "DATE", holder);

                }

//                ChangeDetail(dateString,"DATE",holder);


            }
        });

        builder.show();
    }

    private void TimePicker(final MyViewHolder holder,int type,String Current){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final TimePicker picker = new TimePicker(context);

        String Detail = "";
        switch (type){
            case 2:
                builder.setTitle("Change Start Time");
                Detail = "START_TIME";
                break;
            case 3:
                builder.setTitle("Change End Time");
                Detail = "END_TIME";
                break;
        }

        final String fDetail = Detail;
        builder.setTitle("Change ");
        builder.setView(picker);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*
                Logic to update the information
                 */
                int hour = picker.getHour();
                int min = picker.getMinute();


                String time = hour+":"+min;
                holder.genDetail.setText(time);
                ChangeDetail(time,fDetail,holder);


            }
        });

        builder.show();
    }

    private void ChangeDetail(final String value,final String Detail,final MyViewHolder holder){
        /*
        This functions receives a list of events from the server. It'll then be changed from returning void
        to returning an ArrayList containing event details.
         */

        RequestQueue queue = Volley.newRequestQueue(context);

        SharedPreferences WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
        String url =WebAccess.getString("Root_URL",null)+"?type=SetDetail&dbPass="+"&ChangeEvent" +
                "="+Detail+"&Value="+value+"&E_ID="+EventID;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      //  Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                        Log.d("result",response);
                        if (response.contains("Succesful Update")){
                            //TODO Replace this with a snackbar
                            Toast.makeText(context,"successfully updated",Toast.LENGTH_LONG).show();
                            holder.genDetail.setText(value);
                            if (Detail.equals("DATE")){
                                currentEvent.setDate(value);
                            }
                        }else{
                            Log.d("Actual",response);
                            Toast.makeText(context,"failed to update",Toast.LENGTH_LONG).show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO Replace is with snackbar
              //  Toast.makeText(context,"An error occurred",Toast.LENGTH_LONG).show();
                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(context,"1",Toast.LENGTH_LONG).show();

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(context,"2",Toast.LENGTH_LONG).show();

                } else if (error instanceof ServerError) {
                    Toast.makeText(context,"3",Toast.LENGTH_LONG).show();

                } else if (error instanceof NetworkError) {
                    Toast.makeText(context,"4",Toast.LENGTH_LONG).show();

                } else if (error instanceof ParseError) {
                    Toast.makeText(context,"5",Toast.LENGTH_LONG).show();

                }
            }
        });

        queue.add(stringRequest);

    }
}