package com.example.gatsby;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Random;


public class Registration extends AppCompatActivity {
    private int vCode=0;

    /*
    This is where the user will register to become a "party planner"
    by giving their required details.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        final Button submit =  findViewById(R.id.btnSubmit);
        final Button submitCode =  findViewById(R.id.btnSubmitCode);
        submitCode.setEnabled(false);
        submit.setEnabled(false);

        //All of the edits that we need are here
        final EditText name =   findViewById(R.id.edtName);
        final EditText surname =   findViewById(R.id.edtSurname);
        final EditText email =   findViewById(R.id.edtEmail);
        final EditText password =  findViewById(R.id.edtPassword);
        final EditText pass_check =   findViewById(R.id.edtCPassword);
        final TextView VerifyText =  findViewById(R.id.txtVerify);
        final EditText verifyCode =   findViewById(R.id.edtCode);

        name.requestFocus();
        VerifyText.setVisibility(View.INVISIBLE);
        verifyCode.setVisibility(View.INVISIBLE);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isEmpty(name) || isEmpty(surname) || isEmpty(email) || isEmpty(password) || isEmpty(pass_check)) {
                    submit.setEnabled(false);
                }else {
                    submit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        surname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isEmpty(name) || isEmpty(surname) || isEmpty(email) || isEmpty(password) || isEmpty(pass_check)) {
                    submit.setEnabled(false);
                }else {
                    submit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isEmpty(name) || isEmpty(surname) || isEmpty(email) || isEmpty(password) || isEmpty(pass_check)) {
                    submit.setEnabled(false);
                }else {
                    submit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isEmpty(name) || isEmpty(surname) || isEmpty(email) || isEmpty(password) || isEmpty(pass_check)) {
                    submit.setEnabled(false);
                }else {
                    submit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pass_check.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isEmpty(name) || isEmpty(surname) || isEmpty(email) || isEmpty(password) || isEmpty(pass_check)) {
                    submit.setEnabled(false);
                }else {
                    submit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                //Toast.makeText(getApplicationContext(), "Submit clicked",
                        //Toast.LENGTH_LONG).show();

                String na = name.getText().toString();
                String sna = surname.getText().toString();
                String em = email.getText().toString();
                String pa = password.getText().toString();
                String cpa = pass_check.getText().toString();

                //check for a valid email
                if(!Patterns.EMAIL_ADDRESS.matcher(em).matches()){
                    email.setText("");
                    email.setError("Please Enter a valid email address");
                    return;
                }

                if(!pa.equals(cpa)){

                    pass_check.setText("");
                    pass_check.setError("The passwords don't match");
                    return;

                }


                //sets up verifiaction
                name.setVisibility(View.INVISIBLE);
                email.setVisibility(View.INVISIBLE);
                password.setVisibility(View.INVISIBLE);
                pass_check.setVisibility(View.INVISIBLE);
                surname.setVisibility(View.INVISIBLE);
                submit.setVisibility(View.INVISIBLE);
                VerifyText.setVisibility(View.VISIBLE);
                verifyCode.setVisibility(View.VISIBLE);
                submitCode.setVisibility(View.VISIBLE);
                submitCode.setEnabled(true);
                submit.setEnabled(false);
                final int code = new Random().nextInt(1000000)+2000000;
                String scode=Integer.toString(code);
                vCode=code;
                SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
                String url =WebAccess.getString("Root_URL","NULL")+"?dbPass=&type=SendVerification&name="+na+" "+sna+
                        "&code="+scode+"&email="+em;

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

                StringRequest req = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                Log.d("Registering","Send success");
                                Toast.makeText(getApplicationContext(), "Sent Successfully",
                                        Toast.LENGTH_LONG).show();

                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Registering","An error occurred.|"+error.getMessage()+"|");

                        Toast.makeText(getApplicationContext(), error.getMessage(),
                                Toast.LENGTH_LONG).show();


                    }
                });


                queue.add(req);


            }

        });
        submitCode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final Intent myIntent = new Intent(view.getContext(), Log_In.class);
                //Toast.makeText(getApplicationContext(), "Submit clicked",
                //Toast.LENGTH_LONG).show();
                int co =Integer.parseInt(verifyCode.getText().toString());
                if(co!=vCode){
                    Toast.makeText(getApplicationContext(), "Incorrect Code",Toast.LENGTH_LONG).show();
                    name.setVisibility(View.VISIBLE);
                    email.setVisibility(View.VISIBLE);
                    password.setVisibility(View.VISIBLE);
                    pass_check.setVisibility(View.VISIBLE);
                    surname.setVisibility(View.VISIBLE);
                    submit.setVisibility(View.VISIBLE);
                    VerifyText.setVisibility(View.INVISIBLE);
                    verifyCode.setVisibility(View.INVISIBLE);
                    submitCode.setVisibility(View.INVISIBLE);
                    submitCode.setEnabled(false);
                    submit.setEnabled(true);
                    return;
                }
                String na = name.getText().toString();
                String sna = surname.getText().toString();
                String em = email.getText().toString();
                String cpa = pass_check.getText().toString();

                SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
                String url =WebAccess.getString("Root_URL","NULL")+"?dbPass=&type=Register&name="+na+" "+sna+
                        "&Password="+cpa+"&email="+em;

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

                StringRequest req = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                Log.d("Registering","created successfully");
                                Toast.makeText(getApplicationContext(), "Created Successfully",
                                        Toast.LENGTH_LONG).show();
                                startActivityForResult(myIntent, 0);
                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Registering","An error occurred.|"+error.getMessage()+"|");

                        Toast.makeText(getApplicationContext(), error.getMessage(),
                                Toast.LENGTH_LONG).show();


                    }
                });

                queue.add(req);
            }


        });
    }


    private boolean isEmpty(EditText a) {
        return TextUtils.isEmpty(a.getText().toString());
    }


    // a function that runs many lines of code

}
