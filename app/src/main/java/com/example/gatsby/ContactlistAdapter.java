package com.example.gatsby;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayList;

public class ContactlistAdapter extends RecyclerView.Adapter<ContactlistAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<Guest> guestList;

    ContactlistAdapter(Context context, ArrayList<Guest> Guests){//, String EventID, Events currentEvent) {

        this.context = context;
        guestList = Guests;


    }

    @Override
    public void onBindViewHolder(final @NonNull ContactlistAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(guestList.get(position).getName());

    }

    @Override
    public  @NonNull
    ContactlistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.rv_guestlist,parent,false);


        return new MyViewHolder(v);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView name ;

        MyViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.genDetail);

        }
    }

    @Override
    public int getItemCount() {
        return guestList.size();
    }



}