package com.example.gatsby;
import com.google.gson.annotations.SerializedName;

class Guest  {

    @SerializedName("CONTACT_NAME")
    private String Name;
    @SerializedName("EMAIL")
    private String Email;
    @SerializedName("CONTACT_ID")
    private String cID;
    @SerializedName("BRING")
    private String bring;//what they are bringing to the event

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("STATUS")
    private String status;


    public String getStatus() {
        return status;
    }





    public String getName() {
        return Name;
    }

    public String getEmail() {
        return Email;
    }

    public String getcID() {
        return cID;
    }


    @Override
    public boolean equals(Object obj) {
        boolean retVal = false;

        if (obj instanceof Guest){
            Guest other = (Guest) obj;
            retVal = other.cID.equals(this.cID);
        }

        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.cID != null ? this.cID.hashCode() : 0);
        return hash;
    }

    public String getBring() {
        return bring;
    }

    public void setBring(String bring) {
        this.bring = bring;
    }
}
