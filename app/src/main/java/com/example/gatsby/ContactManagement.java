package com.example.gatsby;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;


public class ContactManagement extends AppCompatActivity {
    ArrayList<Guest> ContactList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_management);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Contact Management");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        FloatingActionButton createEvent = findViewById(R.id.floatingActionButton);
        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                addContact();
            }
        });
        getContacts();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
                //return to the home page when the back button is pressed
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getContacts(){
        ContactList.clear();
        SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url =WebAccess.getString("Root_URL",null)+"?type=SelectAllContacts&dbPass="+"&MemberID="+WebAccess.getInt("UserID",-42);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //


                        Gson gson;
                        Log.d("result",response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("dd-mm-yyyy");
                        gson = gsonBuilder.create();

                        try{
                            JSONArray jsonContactArray = new JSONArray(response);

                            for (int item = 0; item<jsonContactArray.length(); item++){
                                JSONObject Contact = (JSONObject)jsonContactArray.get(item);
                                Guest contact = gson.fromJson(Contact.toString(),Guest.class);
                                ContactList.add(contact);
                                
                            }

                            final RecyclerView contactRV;
                            contactRV = findViewById(R.id.rv_contacts);
                            contactRV.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            contactRV.addItemDecoration(new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL));
                            contactRV.setVisibility(View.VISIBLE);
                            contactRV.setAdapter(new ContactlistAdapter(getApplicationContext(),ContactList));

                        }catch(Exception e){

                            Log.d("Contact Retrieval",e.getMessage()+response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                    Log.d("failed", error.getMessage());

            }
        });

        queue.add(stringRequest);
    }

    public void addContact(){
        /*
        This dialog will pop up giving the user the option to change
        the information
         */


        androidx.appcompat.app.AlertDialog.Builder  dlgContact = new  AlertDialog.Builder(this, R.style.AppTheme);
        final View contactView = getLayoutInflater().inflate(R.layout.contact_layout, null);
        final EditText edtName = contactView.findViewById(R.id.edtName);
        final EditText edtEmail = contactView.findViewById(R.id.edtEmail);

        dlgContact.setView(contactView);

        dlgContact.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final String contactName = edtName.getText().toString();
                final String contactEmail = edtEmail.getText().toString();

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
                String url =WebAccess.getString("Root_URL",null)+"?type=AddContact&MemberID="+WebAccess.getInt("UserID",-42)+
                                "&dbPass=&Name="+contactName+"&Email="+contactEmail;

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("result",response.substring(0,response.length()));

                                if (response.contains("New record created successfully")){
                                    getContacts();

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"An error occurred",Toast.LENGTH_LONG).show();

                        try {
                            Log.d("failed", error.getMessage());
                        }catch(Exception e){
                            Log.d("last", e.getMessage());
                        }
                    }
                });

                queue.add(stringRequest);


            }
        });

        dlgContact.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dlgContact.show();
    }
}
