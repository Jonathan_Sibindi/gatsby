package com.example.gatsby;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textview.MaterialTextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class AdditionalDetailsAdapter extends RecyclerView.Adapter<AdditionalDetailsAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<String> extraDetails;
    private int DresscodeIndex;
    private boolean drinkStatusChanged;
    private String EventID;
    private Events currentEvent;
    AdditionalDetailsAdapter(Context context,ArrayList<String> extraDetails,String EventID,Events currentEvent) {

        this.context = context;
        this.extraDetails = extraDetails;
        this.EventID = EventID;
        this.currentEvent = currentEvent;
    }

    @Override
    public void onBindViewHolder(final @NonNull AdditionalDetailsAdapter.MyViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch(position){
                    case 0:
                        editDialog(extraDetails.get(position),holder);
                        break;
                    case 1:
                        DressCodeDialog(holder.extraDetails.getText().toString(),holder);
                        break;
                    case 2://rsvp
                        DatePicker(holder,holder.extraDetails.getText().toString());
                        break;
                  //
                    case 3:
                        CheckBoxDialog(holder.extraDetails.getText().toString(),holder);
                        break;
                    default:
                        break;


                }

            }
        });
        switch(position){
            case 0:
                holder.Description.setText(context.getString(R.string.parking));
                break;
            case 1:
                holder.Description.setText(context.getString(R.string.dress_code));
                break;
            case 2:
                holder.Description.setText(context.getString(R.string.rsvp));
                break;
            case 3:
                holder.Description.setText(context.getString(R.string.drinks));
                break;

            default:break;
        }

        if (position==3){
            if (extraDetails.get(position).equals("1")){
                holder.extraDetails.setText(context.getString(R.string.yes));

            }else{
                holder.extraDetails.setText(context.getString(R.string.no));

            }
        }else{
            holder.extraDetails.setText(extraDetails.get(position));
        }

    }

    @Override
    public  @NonNull
    AdditionalDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.rv_details,parent,false);


        return new MyViewHolder(v);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView Description,extraDetails ;

        MyViewHolder(View itemView) {
            super(itemView);
            Description = itemView.findViewById(R.id.Description);
            extraDetails = itemView.findViewById(R.id.genDetail);

        }
    }

    @Override
    public int getItemCount() {
        return extraDetails.size();
    }


    private void editDialog(String CurrString,final MyViewHolder holder){
        /*
        This dialog will pop up giving the user the option to change
        the information
         */
        String title,message;
        title = "Parking Details";
        message = "Alter the parking details";

        AlertDialog.Builder  dlgUpdate= new  AlertDialog.Builder(context, R.style.AppTheme);
        final EditText edtUpdate= new EditText(context);
        edtUpdate.setText(CurrString);
        edtUpdate.setSelection(edtUpdate.getText().length());

        dlgUpdate.setMessage(message);
        dlgUpdate.setTitle(title);

        dlgUpdate.setView(edtUpdate);

        dlgUpdate.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*
                Logic to update the information
                 */
                //EventName.text
                holder.extraDetails.setText(edtUpdate.getText().toString());
                ChangeDetail(edtUpdate.getText().toString(),"PARKING",holder);

            }
        });

        dlgUpdate.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dlgUpdate.show();
    }

    private void DatePicker(final MyViewHolder holder,final String current){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final DatePicker picker = new DatePicker(context);
        picker.setCalendarViewShown(false);
        builder.setTitle("Change RSVP Date");
        builder.setView(picker);

        if (currentEvent.getDate()!=null) {

            String someDate = currentEvent.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            try{
                Date date = sdf.parse(someDate);
                picker.setMaxDate(date.getTime());//-10*((long)Math.pow(10,10)));
                Log.d("date", "fine date");
            }catch(Exception e){
                Log.d("wrong date", e.getMessage());
            }


        }

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*
                Logic to update the information
                 */
                int day = picker.getDayOfMonth();
                int month = picker.getMonth();
                int year =  picker.getYear();



                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String dateString = sdf.format(calendar.getTime());
                if (!dateString.equals(current)) {

                        ChangeDetail(dateString, "RSVP", holder);

                }


            }
        });

        builder.show();
    }

    private void DressCodeDialog(final String condition, final MyViewHolder holder){
        // setup the alert builder
        final AlertDialog.Builder DressCode_Dialog = new AlertDialog.Builder(context,R.style.AppTheme);
        DressCode_Dialog.setTitle("Select Dress Code");

        final String[] conditions = {"Formal", "Black Tie","Cocktail", "Smart Casual","Casual","none"};

        int checkedItem = 1;

        switch(condition){
            case "Formal"  :
                checkedItem = 0;
                break;
            case "Black Tie":
                checkedItem = 1;
                break;
            case "Cocktail"  :
                checkedItem =2;
                break;
            case "Smart Casual":
                checkedItem =3;
                break;
            case "Casual"   :
                checkedItem =4;
                break;
            case "none"     :
                checkedItem =5;
                break;


        }

        DressCode_Dialog.setSingleChoiceItems(conditions, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                DresscodeIndex = which;
            }
        });




        DressCode_Dialog.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (condition.equals(conditions[DresscodeIndex])){
                    return;
                }

               // holder.extraDetails.setText(conditions[DresscodeIndex]);
                ChangeDetail(conditions[DresscodeIndex],"Dress_Code",holder);


            }
        });
        DressCode_Dialog.setNegativeButton("Cancel", null);

        DressCode_Dialog.show();
    }

     private void CheckBoxDialog(String current,final MyViewHolder holder){
        // setup the alert builder
        final AlertDialog.Builder drinksDialog = new AlertDialog.Builder(context,R.style.AppTheme);
        drinksDialog.setTitle("Drinks");
        final CheckBox checkBox = new CheckBox(context);
        drinksDialog.setView(checkBox);
        drinksDialog.setMessage("Should guests bring their own drinks?");

        final boolean wasTrue = current.equals("Yes");
        checkBox.setChecked(false);
        if (current.equals("Yes")){
            checkBox.setChecked(true);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                    drinkStatusChanged = (isChecked != wasTrue);
                }
            }
        );
        drinksDialog.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!drinkStatusChanged){
                    return;
                }

                if (wasTrue){
                    holder.extraDetails.setText(context.getString(R.string.no));
                    ChangeDetail("0","DRINKS",holder);
                }else{
                    holder.extraDetails.setText(context.getString(R.string.yes));
                    ChangeDetail("1","DRINKS",holder);

                }


            }
        });
        drinksDialog.setNegativeButton("Cancel", null);

        drinksDialog.show();
    }

    private void ChangeDetail(final String value,final String Detail,final MyViewHolder holder){
        /*
        This functions receives a list of events from the server. It'll then be changed from returning void
        to returning an ArrayList containing event details.
         */

        RequestQueue queue = Volley.newRequestQueue(context);
        SharedPreferences WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
        String url =WebAccess.getString("Root_URL",null)+"?type=SetDetail&dbPass="+"&ChangeEvent" +
                "="+Detail+"&Value="+value+"&E_ID="+EventID;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                        Log.d("result",response);
                        if (response.contains("Succesful Update")){
                            //TODO Replace is with snackbar
                            Toast.makeText(context,"successfully updated",Toast.LENGTH_LONG).show();
                            if (Detail.equals("DRINKS")){
                                if (value.equals("0")){
                                    holder.extraDetails.setText(context.getString(R.string.no));
                                }else{
                                    holder.extraDetails.setText(context.getString(R.string.yes));
                                }

                            }else{
                                holder.extraDetails.setText(value);
                            }
                            if (Detail.equals("RSVP")){
                                currentEvent.setRSVP(value);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO Replace is with snackbar
                Toast.makeText(context,"An error occurred",Toast.LENGTH_LONG).show();
                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);

    }
}