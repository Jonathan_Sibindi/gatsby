package com.example.gatsby;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class GuestlistAdapter extends RecyclerView.Adapter<GuestlistAdapter.MyViewHolder> implements PopupMenu.OnMenuItemClickListener {

    private Context context;
    private ArrayList<Guest> guestList;
    private Events currentEvent;
    private Guest current;
    private int guestpos;
    private int StatusIndex;

    GuestlistAdapter(Context context, ArrayList<Guest> Guests,Events currentEvent){//, String EventID, Events currentEvent) {

        this.context = context;
        guestList = Guests;
        this.currentEvent = currentEvent;

    }

    @Override
    public void onBindViewHolder(final @NonNull GuestlistAdapter.MyViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current = guestList.get(position);

                PopupMenu popup = new PopupMenu(context, v);
                popup.setOnMenuItemClickListener(GuestlistAdapter.this);
                popup.inflate(R.menu.guestlist_popup);
                popup.show();
                guestpos = position;
            }
        });


        holder.name.setText(guestList.get(position).getName());

        switch (guestList.get(position).getStatus()){
            case "Declined":
                holder.background.setBackgroundColor(context.getResources().getColor(R.color.declined,context.getTheme()));
                break;
            case "Tentative":
                holder.background.setBackgroundColor(context.getResources().getColor(R.color.tentative,context.getTheme()));
                break;
            case "Accepted":
                holder.background.setBackgroundColor(context.getResources().getColor(R.color.accepted,context.getTheme()));
                break;
            case "No Reply":
            default:
                holder.background.setBackgroundColor(context.getResources().getColor(R.color.noResponse,context.getTheme()));
                break;
        }

    }

    @Override
    public  @NonNull
    GuestlistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.rv_guestlist,parent,false);


        return new MyViewHolder(v);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView name ;
        LinearLayout background;

        MyViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.genDetail);
            background = itemView.findViewById(R.id.Background);
        }
    }

    @Override
    public int getItemCount() {
        return guestList.size();
    }

    public boolean onMenuItemClick(MenuItem item) {
//        Toast.makeText(context, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
//        final int dis = delEvent;
        switch (item.getItemId()) {
            case R.id.bring:
                    changeBring(current);
                    return true;
            case R.id.status:
                    changeStatus(current);
                    return true;
            default:
                return false;

        }

    }

    private void changeStatus(final Guest guest){

        // setup the alert builder
        final AlertDialog.Builder Status_Dialog = new AlertDialog.Builder(context,R.style.AppTheme);
        Status_Dialog.setTitle("Select Dress Code");

        final String[] statuses = {"Accepted", "Declined","Tentative", "No Reply"};

        int checkedItem = 1;

        switch(guest.getStatus()){
            case "Accepted"  :
                checkedItem = 0;
                break;
            case "Declined":
                checkedItem = 1;
                break;
            case "Tentative"  :
                checkedItem =2;
                break;
            case "No Reply":
            default:
                checkedItem =3;
                break;



        }

        Status_Dialog.setSingleChoiceItems(statuses, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StatusIndex = which;
            }
        });




        Status_Dialog.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (guest.getStatus().equals(statuses[StatusIndex])){//i.e. no change
                    return;
                }

                // holder.extraDetails.setText(conditions[DresscodeIndex]);
                updateStatus(current,statuses[StatusIndex]);


            }
        });
        Status_Dialog.setNegativeButton("Cancel", null);

        Status_Dialog.show();
    }
    private void updateStatus(final Guest guest,final String status){
        final int pos = guestpos;
        RequestQueue queue = Volley.newRequestQueue(context);
        SharedPreferences WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
        String url =WebAccess.getString("Root_URL",null)+"?type=ChangeStatus&GUEST_ID="+guest.getcID()+
                "&Event_ID="+currentEvent.getId()+"&Status="+status+"&dbPass=";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);

                        if (response.contains("Record updated succesfully")) {
                            Toast.makeText(context, "Status Updated", Toast.LENGTH_LONG).show();
                            guestList.get(pos).setStatus(status);
                            notifyDataSetChanged();
                        }else{

                        }

                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO Replace is with snackbar

                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);
    }
    private void changeBring(final Guest guest){//change the item that the guest should bring


        /*
        This dialog will pop up giving the user the option to change
        the information
         */
            String title,message;
            title = "What to bring";
            message = "Please specify what the guest should bring";

            AlertDialog.Builder  dlgUpdate= new  AlertDialog.Builder(context, R.style.AppTheme);
            final EditText edtUpdate= new EditText(context);
            if (guest.getBring() != null){
                edtUpdate.setText(guest.getBring());
                edtUpdate.setSelection(edtUpdate.getText().length());
            }


            dlgUpdate.setMessage(message);
            dlgUpdate.setTitle(title);

            dlgUpdate.setView(edtUpdate);


            dlgUpdate.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    final String text = edtUpdate.getText().toString();
                    RequestQueue queue = Volley.newRequestQueue(context);
                    SharedPreferences WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
                    String url =WebAccess.getString("Root_URL",null)+"?type=AddBring&dbPass=&GUEST_ID="+guest.getcID()+
                            "&Event_ID="+currentEvent.getId()+"&BringItem="+text;
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("response", response);

                                    if (response.contains("Record updated succesfully")) {
                                        Toast.makeText(context, "Item successfully changed", Toast.LENGTH_LONG).show();

                                        for (Guest g : guestList) {
                                            //change the value in the object if the server side was successfully updated

                                            if (g.equals(guest)) {
                                                g.setBring(text);
                                            }
                                        }
                                    }

                                }


                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //TODO Replace is with snackbar

                            try {
                                Log.d("failed", error.getMessage());
                            }catch(Exception e){
                                Log.d("last", e.getMessage());
                            }
                        }
                    });

                    queue.add(stringRequest);


                }
            });

            dlgUpdate.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                }
            });
            dlgUpdate.show();



    }








}