package com.example.gatsby;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class EventsAdapter  extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> implements PopupMenu.OnMenuItemClickListener{

    private Context context;
    private ArrayList<Events> Events;
    private int delEvent;
    private ArrayList<Guest> guestList = new ArrayList<>();

    public EventsAdapter(Context context,ArrayList<Events> Events) {

        this.context = context;
        this.Events = Events;

    }

    @Override
    public void onBindViewHolder(final @NonNull EventsAdapter.MyViewHolder holder, final int position) {
            holder.Initial.setText(Events.get(position).getName());
            holder.Name.setText(Events.get(position).getName().substring(0,1));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(context, EventDetails.class);
                    myIntent.putExtra("EventDetails",Events.get(position).getJsonDetails());
                    context.startActivity(myIntent);

                }
            });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Toast.makeText(context, "It works", Toast.LENGTH_SHORT).show();
                PopupMenu popup = new PopupMenu(context, v);
                popup.setOnMenuItemClickListener(EventsAdapter.this);
                popup.inflate(R.menu.popup_menu);
                popup.show();
                delEvent = position;


                return true;
            }
        });
    }
    @Override
    public  @NonNull
    EventsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.rv_events,parent,false);


        return new MyViewHolder(v);
    }


     public class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView Name , Initial;

        public MyViewHolder(View itemView) {
            super(itemView);
            Name = itemView.findViewById(R.id.EventName);
            Initial = itemView.findViewById(R.id.EventDetail);
        }
    }

    @Override
    public int getItemCount() {
        return Events.size();
    }

    public boolean onMenuItemClick(MenuItem item) {
        final int dis = delEvent;
        final SharedPreferences WebAccess;
        String url;
        RequestQueue queue;
        StringRequest req;
        switch (item.getItemId()) {
            case R.id.delete_item:
                //DO the thing with delEvent
                WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
                url =WebAccess.getString("Root_URL",null)+"?dbPass=&type=DeleteEvent&EventID="+Events.get(dis).getId();
                queue = Volley.newRequestQueue(context);

                req = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                //  Log.d("Deleting","Deleted successfully");
                                Log.d("kkkk",response);
                                Events.remove(dis);
                                notifyItemRemoved(dis);
                                Toast.makeText(context, "Deleted Successfully",
                                        Toast.LENGTH_LONG).show();
                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Registering","An error occurred.|"+error.getMessage()+"|");

                        Toast.makeText(context, error.getMessage(),
                                Toast.LENGTH_LONG).show();


                    }
                });

//                req.setRetryPolicy(new DefaultRetryPolicy(
//                        30000,
//                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(req);
                return true;
            case R.id.send_invites:
                WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
                String namei = WebAccess.getString("UserName", "NULL");
                String [] boii = namei.split(" ");
                url =WebAccess.getString("Root_URL",null)+"?type=SendInvites"+
                        "&dbPass=&EventID="+Events.get(dis).getId()+"&Username="+boii[0];


                queue = Volley.newRequestQueue(context);

                req = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Message has been sent")){
                                    Toast.makeText(context,"Invites Successfully Sent",Toast.LENGTH_LONG).show();
                                }else{

                                    Log.d("invite error", response);
                                    Toast.makeText(context,"An Error Occurred please try again later",Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context,"An Error Occurred please try again later",Toast.LENGTH_LONG).show();


                        try {
                            Log.d("failed", error.getMessage());
                        }catch(Exception e){
                            Log.d("last", e.getMessage());
                        }
                    }
                });

                queue.add(req);
                return true;
            case R.id.send_rsvp:
                WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
                String namer = WebAccess.getString("UserName", "NULL");
                String [] boir = namer.split(" ");
                url =WebAccess.getString("Root_URL",null)+"?type=SendRSVP" +
                        "&dbPass=&EventID="
                        +Events.get(dis).getId()+
                        "&Username="+boir[0];


                queue = Volley.newRequestQueue(context);

                req = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.contains("Message has been sent")){
                                    Toast.makeText(context,"Reminders Successfully Sent",Toast.LENGTH_LONG).show();
                                }else{

                                    Log.d("rsvperror", response);
                                    Toast.makeText(context,"An Error Occurred please try again later",Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context,"An Error Occurred please try again later",Toast.LENGTH_LONG).show();


                        try {
                            Log.d("failed", error.getMessage());
                        }catch(Exception e){
                            Log.d("last", e.getMessage());
                        }
                    }
                });

                queue.add(req);
                return true;

            case R.id.send_sample:

                queue = Volley.newRequestQueue(context);
                WebAccess = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
                url =WebAccess.getString("Root_URL",null)+"?type=SelectEventDetails&dbPass=&EventID="+Events.get(dis).getId();
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //


                                Gson gson;
                                Log.d("result",response);
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                gsonBuilder.setDateFormat("dd-mm-yyyy");
                                gson = gsonBuilder.create();


                                try{
                                    JSONArray jsonGuestArray = new JSONArray(response);/*"[{\"CONTACT_NAME\":\"Agang\",\n" +
                                                                    "\"SURNAME\":\"Lebethe\"\n" +
                                                                    "},\n" +
                                                                    "{\"NAME\":\"David\",\n" +
                                                                    "\"SURNAME\":\"Wright\"\n" +
                                                                    "}\n" +
                                                                    "]");//response);*/
                                    guestList.clear();
                                    for (int item = 0; item<jsonGuestArray.length(); item++){
                                        JSONObject Event = (JSONObject)jsonGuestArray.get(item);
                                        Guest newGuest = gson.fromJson(Event.toString(),Guest.class);
                                        guestList.add(newGuest);

                                    }

                                    SharedPreferences WebAccess1 = context.getSharedPreferences("Web_Access",MODE_PRIVATE);
                                    String name = WebAccess1.getString("UserName", "NULL");
                                    String [] boi = name.split(" ");
                                    String url1 =WebAccess1.getString("Root_URL",null)+"?type=SendSampleEmail&"+
                                            "&dbPass=&EventID="+Events.get(dis).getId()+"&email="+WebAccess1.getString("Email", "NULL")
                                            +"&email="+WebAccess1.getString("Email", "NULL")+"&Username="+boi[0]
                                            +"&GuestID="+guestList.get(0).getcID();
                                    RequestQueue q =  Volley.newRequestQueue(context);
                                    System.out.println(url1);
                                    StringRequest req1 =  new StringRequest(Request.Method.POST, url1,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {

                                                    if (response.contains("Message has been sent")){
                                                        Toast.makeText(context,"Message has been sent",Toast.LENGTH_LONG).show();
                                                    }else{
                                                        //
                                                        Log.d("jjjjj:", response);
                                                        Toast.makeText(context,"An Error Occurred please try again later I am here",Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(context,"An Error Occurred please try again later ",Toast.LENGTH_LONG).show();


                                            try {
                                                Log.d("failed", error.getMessage());

                                            }catch(Exception e){
                                                Log.d("last", e.getMessage());
                                            }
                                        }
                                    });

                                    q.add(req1);

                                }catch(Exception e){
                                    // Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();
                                    Log.d("problem13",e.getMessage());
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //TODO Replace this with snackbar
//                Toast.makeText(getContext(),"An error occurred",Toast.LENGTH_LONG).show();
                        try {
                            Log.d("failed", error.getMessage());
                        }catch(Exception e){
                            Log.d("last", e.getMessage());
                        }
                    }
                });

                queue.add(stringRequest);

                return true;
            default:
                return false;

        }
    }
}