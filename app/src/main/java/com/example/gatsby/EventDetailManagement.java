package com.example.gatsby;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class EventDetailManagement extends AppCompatActivity {
    TextInputLayout EventName,Name,Date,StartTime,EndTime,Parking;
    CheckBox Drinks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail_management);
        Name = findViewById(R.id.edtName);
        Date = findViewById(R.id.edtDate);
        StartTime = findViewById(R.id.edtStartTime);
        EndTime = findViewById(R.id.edtEndTime);
        Parking = findViewById(R.id.edtParkingDetails);
        Drinks = findViewById(R.id.cbDrinks);
        EventName = findViewById(R.id.edtEventName);

        /*
        * Use the data passed through the intent extras to populate the edit texts
        * over here
        */
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Event Name");
        try{
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }catch(Exception e){
            Log.d("Setting icon","error showing back arrow icon");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //return to the home page when the back button is pressed
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    
    public void editDialog(String CurrString,String Code){
        /*
        This dialog will pop up giving the user the option to change
        the information
         */
        String title,message;
        switch (Code){

            default:
                title = "heading";
                message = "instructions";
                break;
        }
        AlertDialog.Builder  dlgUpdate= new  AlertDialog.Builder(this, R.style.AppTheme);
        final EditText edtUpdate= new EditText(this);
        edtUpdate.setText(CurrString);
        edtUpdate.setSelection(edtUpdate.getText().length());

        dlgUpdate.setMessage(message);
        dlgUpdate.setTitle(title);

        dlgUpdate.setView(edtUpdate);

        dlgUpdate.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                /*
                Logic to update the information
                 */
                //EventName.text
            }
        });

        dlgUpdate.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dlgUpdate.show();
    }

    public void UpdateName(View v){
        editDialog("-----","EDT_WHATEVER");
    }
}
