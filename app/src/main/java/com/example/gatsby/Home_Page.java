package com.example.gatsby;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;


public class Home_Page extends AppCompatActivity {

    /*
    From this page the user will be able to see a list of all their
    events. They can click on the event for further options, or they
    can proceed to add an event as needed.
     */

    private DrawerLayout mDrawerLayout;
    private Gson gson;
    ArrayList<Events> EventList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home__page);
        EventList = new ArrayList<>();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Your events");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.profile:
                                //add profile logic here
                                break;
                            case R.id.contacts:

                                Intent myIntent = new Intent(getApplicationContext(), ContactManagement.class);
                                startActivity(myIntent);
                                break;
                            case R.id.settings:
                                //add settings logic here
                                break;
                            case R.id.logout:
                                SharedPreferences.Editor editor = getSharedPreferences("Web_Access",MODE_PRIVATE).edit();
                                editor.putInt("UserID",-42);
                                editor.putBoolean("LoggedIN",false);
                                editor.putString("Email","none");
                                editor.putString("UserName","none");
                                editor.commit();//we do this so that it takes us back to the LOG in screen only when it is done saving
                                finish();

                                break;
                            default:
                                break;
                        }

                        mDrawerLayout.closeDrawers();


                        return true;
                    }
                });

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("dd-mm-yyyy");
        gson = gsonBuilder.create();
        ShowEvents();

        FloatingActionButton createEvent = findViewById(R.id.floatingActionButton);
        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                createDialog();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void ShowEvents(){
        //This function is used to populate the recycler view
        GetEvents();

    }


    public void GetEvents(){
        /*
        This functions receives a list of events from the server. It'll then be changed from returning void
        to returning an ArrayList containing event details.
         */
        SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url =WebAccess.getString("Root_URL",null)+"?type=SelectAllEvent&MemberID="+WebAccess.getInt("UserID",-42)+"&dbPass=";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(getApplicationContext(),response.substring(0,response.length()-1),Toast.LENGTH_LONG).show();
                        Log.d("result",response.substring(0,response.length()-1));
                        EventList.clear();
                        try {

                            JSONArray jsonEventArray = new JSONArray(response);
                            for (int item = 0; item<jsonEventArray.length(); item++){
                                JSONObject Event = (JSONObject)jsonEventArray.get(item);
                                Events newEvent = gson.fromJson(Event.toString(), Events.class);
                                newEvent.setJsonDetails(Event.toString());
                                EventList.add(newEvent);
                            }
                        }catch(Exception e){
                            Log.d("problem",e.getMessage());
                        }

                        final RecyclerView BookingsRecyclerView;
                        BookingsRecyclerView = findViewById(R.id.provider_requests_rv);
                        BookingsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                        BookingsRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL));
                        BookingsRecyclerView.setVisibility(View.VISIBLE);
                        BookingsRecyclerView.setAdapter(new EventsAdapter(getApplicationContext(),EventList));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"An error occurred",Toast.LENGTH_LONG).show();

                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);

    }

    public void editDialog(){
        /*
        This dialog will pop up giving the user the option to change
        the information
         */


        AlertDialog.Builder  dlgUpdate= new  AlertDialog.Builder(this, R.style.AppTheme);
        final EditText edtUpdate= new EditText(this);
        edtUpdate.setSelection(edtUpdate.getText().length());

        // the name of the event is stored here.

        dlgUpdate.setMessage("Enter the event name");
        dlgUpdate.setTitle("Create Event");

        dlgUpdate.setView(edtUpdate);

        dlgUpdate.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            final String eventName = edtUpdate.getText().toString();
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);

            String url =WebAccess.getString("Root_URL",null)+"?type=CreateEvent&MemberID="+WebAccess.getInt("UserID",-42)+"&dbPass=&EventName="+eventName;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.wtf("result!!","|"+response+"|");
                            if (response.contains("New record created successfully")){

                                GetEvents2();
                               // Toast.makeText(getApplicationContext(),"got called!!!",Toast.LENGTH_LONG).show();
                            }else{
                                //Toast.makeText(getApplicationContext(),"damn",Toast.LENGTH_LONG).show();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),"An error occurred",Toast.LENGTH_LONG).show();

                    try {
                        Log.d("failed", error.getMessage());
                    }catch(Exception e){
                        Log.d("last", e.getMessage());
                    }
                }
            });

                queue.add(stringRequest);


            }
        });

        dlgUpdate.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dlgUpdate.show();
    }

    public void createDialog() {
        editDialog();
    }


    @Override
    public void onResume(){
        super.onResume();
        ShowEvents();
    }

    public void GetEvents2(){

        /*
        This functions receives a list of events from the server after creating an event (this is to ensure that you
        are not editing an event that isn't inserted in the server
         */


        SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url =WebAccess.getString("Root_URL",null)+"?type=SelectAllEvent&MemberID="+WebAccess.getInt("UserID",-42)+"&dbPass=";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(getApplicationContext(),response.substring(0,response.length()-1),Toast.LENGTH_LONG).show();
                        Log.d("result",response.substring(0,response.length()-1));
                        EventList.clear();
                        try {

                            JSONArray jsonEventArray = new JSONArray(response);
                            for (int item = 0; item<jsonEventArray.length(); item++){
                                JSONObject Event = (JSONObject)jsonEventArray.get(item);
                                Events newEvent = gson.fromJson(Event.toString(), Events.class);
                                newEvent.setJsonDetails(Event.toString());
                                EventList.add(newEvent);
                            }
                            final RecyclerView BookingsRecyclerView;
                            BookingsRecyclerView = findViewById(R.id.provider_requests_rv);
                            BookingsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                            BookingsRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL));
                            BookingsRecyclerView.setVisibility(View.VISIBLE);
                            BookingsRecyclerView.setAdapter(new EventsAdapter(getApplicationContext(),EventList));
                            Intent myIntent = new Intent(getApplicationContext(), EventDetails.class);
                            myIntent.putExtra("EventDetails",EventList.get(EventList.size()-1).getJsonDetails());
                            startActivity(myIntent);
                        }catch(Exception e){
                            Log.d("problem",e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"An error occurred",Toast.LENGTH_LONG).show();

                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);

    }

}
