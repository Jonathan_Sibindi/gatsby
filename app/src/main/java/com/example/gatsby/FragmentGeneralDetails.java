package com.example.gatsby;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragmentGeneralDetails extends Fragment {
    private static final String TAG = "General Details tab";


    public FragmentGeneralDetails() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_general_details,container,false);
        ArrayList<String> EventDetails = new ArrayList<>();
        Events currentEvent;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson;

        gsonBuilder.setDateFormat("dd-mm-yyyy");
        gson = gsonBuilder.create();
        try{
            String Event = getArguments().getString("event");
            currentEvent = gson.fromJson(Event.toString(), Events.class);
            currentEvent.setJsonDetails(Event);
        }catch (Exception e){
            return view;
        }

        EventDetails.add(currentEvent.getName());
        EventDetails.add(currentEvent.getDate());
        EventDetails.add(currentEvent.getStartTime());
        EventDetails.add(currentEvent.getEndTime());
        EventDetails.add(currentEvent.getVenue());
        final RecyclerView GenDetailsRV;
        GenDetailsRV = view.findViewById(R.id.gen_details_rv);
        GenDetailsRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        GenDetailsRV.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
        GenDetailsRV.setVisibility(View.VISIBLE);
        GenDetailsRV.setAdapter(new GenDetailsAdapter(getActivity(),EventDetails,Integer.toString(currentEvent.getId()),currentEvent));

        return view;

    }



}

