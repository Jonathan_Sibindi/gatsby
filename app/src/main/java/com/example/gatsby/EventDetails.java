package com.example.gatsby;


import android.app.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;


public class EventDetails extends AppCompatActivity {
    private static final String TAG = "Event Details";
    private SectionPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;
    private Gson gson;
    private Events CurrentEvent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        Log.d(TAG,"on create starting");
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("dd-mm-yyyy");
        gson = gsonBuilder.create();

        Bundle extras = getIntent().getExtras();
        try{

            String Event = extras.getString("EventDetails");
            CurrentEvent = gson.fromJson(Event.toString(), Events.class);
            CurrentEvent.setJsonDetails(Event);
        }catch(Exception e){
            //If we can't extract the event details then close
            finish();
        }
        //This is the change we need for a commit
//        try {
//
//            JSONArray jsonEventArray = new JSONArray("[{\"EVENT_NAME\":\"party\",\n" +
//                    "  \"EVENT_ID\":\"1\",\n" +
//                    "  \"MEMBER_ID\":\"1\",\n" +
//                    "  \"START_TIME\":\"12:00\",\n" +
//                    "  \"END_TIME\":\"18:00\",\n" +
//                    "  \"VENUE\":\"WSS1\",\n" +
//                    "  \"DRESS_CODE\":\"Smart Casual\",\n" +
//                    "  \"PARKING\":\"Use First year parking\",\n" +
//                    "  \"RSVP\":\"21-08-2019\",\n" +
//                    "  \"DATE\":\"10-08-2019\",\n" +
//                    "  \"DRINKS\":\"1\"\n" +
//                    "}\n" +
//                    "]");//response.substring(0,response.length()-1));
//
//                JSONObject Event = (JSONObject)jsonEventArray.get(0);
//                CurrentEvent = gson.fromJson(Event.toString(), Events.class);
//                CurrentEvent.setJsonDetails(Event.toString());
//
//
//        }catch(Exception e){
//            finish();
//            Log.d("problem",e.getMessage());
//        }


     //   Log.d("problem",extras.getString("EventDetails"));
        mSectionsPageAdapter = new SectionPageAdapter(getSupportFragmentManager(),3);
        mViewPager = findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tableLayout = findViewById(R.id.tabs);
        tableLayout.setupWithViewPager(mViewPager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Event Name");
        try{
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }catch(Exception e){
            Log.d("Setting icon","error showing back arrow icon");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }


    private void setupViewPager(ViewPager viewPager){
        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager(),3);
        Fragment genDetails = new FragmentGeneralDetails();
        Fragment moreDetails = new FragmentAdditionalDetails();
        Fragment guestList = new FragmentGuestList();
        Bundle args = new Bundle();
        args.putString("event", CurrentEvent.getJsonDetails());
        genDetails.setArguments(args);
        moreDetails.setArguments(args);
        guestList.setArguments(args);
        adapter.addFragment(genDetails,"General");
        adapter.addFragment(moreDetails,"Additional");
        adapter.addFragment(guestList,"Guest List");
        viewPager.setAdapter(adapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //return to the home page when the back button is pressed
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
