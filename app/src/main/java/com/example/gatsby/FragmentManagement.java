package com.example.gatsby;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class FragmentManagement extends Fragment {
    private static final String TAG = "Additional Details tab";
    private ArrayList<Guest> guestList = new ArrayList<>();
    private Events currentEvent;
    private String[]  OptionsArray = {"Send Preview Email","Send Invite","Send RSVP reminder"};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final  View view2 = inflater.inflate(R.layout.fragment_management,container,false);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson;

        gsonBuilder.setDateFormat("dd-mm-yyyy");
        gson = gsonBuilder.create();

        try{
            if (getArguments() != null) {//this needs to be here otherwise a null pointer exception will always be triggered
                String Event = getArguments().getString("event");
                currentEvent = gson.fromJson(Event.toString(), Events.class);
                currentEvent.setJsonDetails(Event);

            }

        }catch (Exception e){
            Log.d("something",e.getMessage());

//            Toast.makeText(getActivity(),"guest page problem",Toast.LENGTH_LONG).show();
            return view2;
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.fragment_management,OptionsArray);

        ListView listView =  view2.findViewById(R.id.lvOptions);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position){
                    case 0:
                        SendEmailPreview();
                        break;
                    case 1:
                        SendInvite();
                        break;
                    case 2:
                        SendRSVPReminder();
                        break;
                    default:
                        break;
                }

            }
        });

        return view2;
    }

    private void SendEmailPreview(){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        SharedPreferences WebAccess = getActivity().getSharedPreferences("Web_Access",MODE_PRIVATE);

        String url =WebAccess.getString("Root_URL",null)+"?type=SendEmailPreview="+WebAccess.getInt("UserID",-42)+
                "&dbPass=&EventID="+currentEvent.getId();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Preview Sent")){
                            Toast.makeText(getContext(),"Preview Successfully Sent",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext(),"An Error Occurred please try again later",Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"An Error Occurred please try again later",Toast.LENGTH_LONG).show();


                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);
    }

    private void SendInvite(){

    }

    private void SendRSVPReminder(){

    }

}

