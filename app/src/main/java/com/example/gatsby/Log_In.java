package com.example.gatsby;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

public class Log_In extends AppCompatActivity {
    /*
    The page used by a party planner to log in to their account
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log__in);

        SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences("Web_Access",MODE_PRIVATE).edit();


        if(!WebAccess.contains("LoggedIN")){

            //This is to set the app lock
            editor.putBoolean("LoggedIN",false);
            editor.apply();
        }else if (WebAccess.getBoolean("LoggedIN",true)){
            Intent myIntent = new Intent(getApplicationContext(), Home_Page.class);
            startActivityForResult(myIntent, 0);
        }

        if(!WebAccess.contains("UserID")){

            editor.putInt("UserID",-42);
            editor.apply();
        }

        if(!WebAccess.contains("UserName")){

            editor.putString("UserID","none");
            editor.apply();
        }

        SharedPreferences.Editor url_editor = getSharedPreferences("Web_Access",MODE_PRIVATE).edit();
        url_editor.putString("Root_URL","http://lamp.ms.wits.ac.za/~s1610338/index.php");//"http://10.0.2.2/Gatsby/index.php");
        url_editor.apply();

        Button login = (Button) findViewById(R.id.log_in);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditText userEmail = findViewById(R.id.user_email);
                EditText Password = findViewById(R.id.password);

                login(userEmail.getText().toString(),Password.getText().toString());

//                Intent myIntent = new Intent(view.getContext(), Home_Page.class);
//                startActivityForResult(myIntent, 0);
            }

        });
        Button signup = (Button) findViewById(R.id.btnSignUp);
        signup.setBackgroundColor(Color.WHITE);
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Registration.class);
                startActivityForResult(myIntent, 0);
            }

        });
    }

    public void login(String username,String password){


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
        String url =WebAccess.getString("Root_URL",null)+"?type=login&dbPass=&email="+username+"&Password="+password;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("result::",response);
                        if (response.contains("Incorrect Login")){

                            final EditText userEmail = findViewById(R.id.user_email);
                            final EditText Password = findViewById(R.id.password);
                            Password.setText("");
                            userEmail.setError(response);

                        }else{

                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.setDateFormat("dd-mm-yyyy");
                            Gson gson = gsonBuilder.create();

                            try{
                                JSONArray jsonMemberArray = new JSONArray(response);
                                for (int item = 0; item<1; item++) {
                                    JSONObject Member = (JSONObject)(jsonMemberArray.get(item));
                                    Member newMember = gson.fromJson(Member.toString(), Member.class);
                                    newMember.setJson(response);


                                    SharedPreferences.Editor editor = getSharedPreferences("Web_Access", MODE_PRIVATE).edit();
                                    editor.putBoolean("LoggedIN", true);
                                    editor.putInt("UserID", newMember.getId());
                                    editor.putString("UserName", newMember.getName());
                                    editor.putString("Email", newMember.getEmail());
                                    editor.apply();

                                    final EditText userEmail = findViewById(R.id.user_email);
                                    final EditText Password = findViewById(R.id.password);
                                    userEmail.setText("");
                                    Password.setText("");


                                    Intent myIntent = new Intent(getApplicationContext(), Home_Page.class);
                                    startActivityForResult(myIntent, 0);
                                }
                            }catch(Exception e){
                                Toast.makeText(getApplicationContext(),"Incorrect Login",Toast.LENGTH_LONG).show();
                                Log.d("rando::",e.getMessage());
                            }



                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"An error occurred",Toast.LENGTH_LONG).show();

                try {
                    Log.d("failed", error.getMessage());
                }catch(Exception e){
                    Log.d("last", e.getMessage());
                }
            }
        });

        queue.add(stringRequest);
    }
    @Override
    public void onStart(){
        super.onStart();



    }

    @Override
    public void onResume(){
        super.onResume();

        SharedPreferences WebAccess = getSharedPreferences("Web_Access",MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences("Web_Access",MODE_PRIVATE).edit();


        if(WebAccess.getBoolean("LoggedIN",true)){
            finish();
        }
    }
}
